module.exports = (db, DataType) => {
  const Role = db.define('role', {
    id: {
      type: DataType.INTEGER.UNSIGNED,
      primaryKey: true
    }
  })

  Role.references = {
    user: {
      type: 'belongsToMany',
      model: 'user',
      options: {
        through: 'userRoles'
      }
    }
  }

  return Role
}
