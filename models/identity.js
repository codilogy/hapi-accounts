const {hash, random} = require('../commons')

module.exports = (db, DataType) => {
  const Identity = db.define('identity', {
    provider: {
      type: DataType.STRING,
      description: 'facebook, google, twitter, linkedin',
      allowNull: false,
      defaultValue: 'email',
      unique: 'identity'
    },
    providerId: {
      type: DataType.STRING,
      allowNull: true,
      description: 'The provider specific id'
    },
    email: {
      type: DataType.STRING,
      allowNull: false,
      unique: 'identity'
    },
    emailIsVerified: {
      type: DataType.BOOLEAN,
      defaultValue: false
    },
    emailConfirmationCode: {
      type: DataType.STRING,
      defaultValue: () => hash(random(8))
    },
    profile: {
      type: DataType.TEXT
    },
    credentials: {
      type: DataType.TEXT
    }
  })

  Identity.references = {
    user: {
      type: 'belongsTo',
      model: 'user'
    }
  }

  return Identity
}
