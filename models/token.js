const jwt = require('jsonwebtoken')
// const {random} = require('../commons')

module.exports = (db, DataType) => {
  const Token = db.define('token', {
    id: {
      type: DataType.INTEGER.UNSIGNED,
      autoIncrement: true,
      primaryKey: true
    },
    accessToken: {
      type: DataType.TEXT,
      allowNull: false
    }
  })

  Token.generateToken = (uid, key) => jwt.sign({uid}, key, {
    algorithm: 'HS256',
    expiresIn: '14 days'
  })

  Token.references = {
    user: {
      type: 'belongsTo',
      model: 'user'
    }
  }

  return Token
}
