module.exports = (db, DataType) => {
  const User = db.define('user', {
    id: {
      type: DataType.INTEGER.UNSIGNED,
      autoIncrement: true,
      primaryKey: true
    },
    password: {
      type: DataType.TEXT
    }
  })

  User.prototype.issueToken = function () {
    const Token = db.models.token

    if (!Token.options.jwtKey) {
      throw Error('Cannot issue a token without JWT key')
    }

    return Token.create({
      userId: this.id,
      accessToken: Token.generateToken(this.id, Token.options.jwtKey)
    })
  }

  User.prototype.verifyPassword = function (password) {
    return password === this.getDataValue('password')
  }

  User.references = {
    identity: {
      type: 'hasMany',
      model: 'identity'
    },
    token: {
      type: 'hasMany',
      model: 'token'
    },
    role: {
      type: 'belongsToMany',
      model: 'role',
      options: {
        through: 'userRoles'
      }
    }
  }

  User.remoteMethods = require('../models-remoting/user')

  return User
}
