const crypto = require('crypto')

module.exports = (value, algorithm = 'sha256') =>
  crypto.createHash(algorithm).update(value).digest('hex')
