module.exports = (data, keys) => Object.keys(data).reduce((result, key) => {
  if (keys.includes(key)) {
    result[key] = data[key]
  }
  return result
}, Object.create(null))
