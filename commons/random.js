const crypto = require('crypto')

module.exports = (length, encoding = 'hex') => crypto.randomBytes(length).toString(encoding)
