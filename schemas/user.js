const Joi = require('joi')

module.exports = Joi.object().keys({
  id: Joi.number().integer().positive().optional(),
  password: Joi.string().trim().min(8).max(32).optional(),
  email: Joi.string().trim().email().required(),
  emailIsVerified: Joi.boolean().optional(),
  emailVerificationToken: Joi.string().trim().min(64).max(128).optional()
})
