const Joi = require('joi')

module.exports = Joi.object().keys({
  email: Joi.string().trim().email().required(),
  provider: Joi.string().optional().default('email'),
  password: Joi.string().optional()
})
