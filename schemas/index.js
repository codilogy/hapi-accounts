exports.UserSchema = require('./user')
exports.CredentialSchema = require('./credential')
exports.ActivationSchema = require('./activation')
