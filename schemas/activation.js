const Joi = require('joi')

module.exports = Joi.object().keys({
  identityId: Joi.number().min(1).required(),
  emailConfirmationCode: Joi.string().required()
})
