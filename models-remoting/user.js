const Boom = require('boom')
const {UserSchema, CredentialSchema, ActivationSchema} = require('../schemas')
const {pick, random, hash} = require('../commons')

const checkIfInstanceExists = identity => {
  if (!identity) {
    throw Boom.notFound('Account not found')
  }

  return identity
}

const checkIfEmailIsVerified = (status = true) => identity => {
  if (identity.emailIsVerified !== status) {
    throw Boom.expectationFailed(status === true
      ? 'Account has not been activated yet'
      : 'Account has been activated already'
    )
  }

  return identity
}

const checkConfirmationCode = emailConfirmationCode => identity => {
  if (identity.emailConfirmationCode !== emailConfirmationCode) {
    throw Boom.badRequest('Invalid confirmation code')
  }

  return identity
}

const issueNewPassword = (server) => identity => {
  const password = random(8)

  return server.password.encrypt(password).then(encryptedPassword => {
    identity.user.password = encryptedPassword

    return identity.user.save()
  }).tap(() => {
    server.sendMail('account-password', {
      subject: 'Account password',
      email: identity.email,
      data: { password }
    })
  })
}

module.exports = (server, db, User) => {
  const Identity = db.models.identity

  server.route({
    method: ['PUT'],
    path: `/${User.tableName}/register`,
    config: {
      auth: false,
      validate: {
        payload: UserSchema
      },
      handler: (request, reply) => {
        return Identity.create(request.payload).then(identity => {
          return User.create({})
            .then(user => identity.setUser(user))
        })
        .tap(identity => {
          server.sendMail('confirm-registration', {
            subject: 'Confirm registration', // TODO: Implement languages (as server service?)
            email: identity.email,
            data: { identity }
          })
        })
        .then(() => reply().code(204))
        .catch(error => {
          let boomError
          if (error.name === 'SequelizeUniqueConstraintError') {
            boomError = Boom.conflict(error.message)
            boomError.output.payload.errors = error.errors
          } else {
            boomError = Boom.internal(error.message)
          }

          reply(boomError)
        })
      }
    }
  })

  server.route({
    method: ['POST'],
    path: `/${User.tableName}/activate/{identityId}/{emailConfirmationCode}`,
    config: {
      auth: false,
      validate: {
        params: ActivationSchema
      },
      handler: (request, reply) => {
        Identity.findByPrimary(request.params.identityId, { include: [ User ] })
          .then(checkIfInstanceExists)
          .then(checkIfEmailIsVerified(false))
          .then(checkConfirmationCode(request.params.emailConfirmationCode))
          .tap(identity => {
            identity.emailIsVerified = true
          })
          .call('save')
          .then(issueNewPassword(server))
          .then(() => reply().code(204))
          .catch(reply)
      }
    }
  })

  server.route({
    method: ['POST'],
    path: `/${User.tableName}/login`,
    config: {
      auth: false,
      validate: {
        payload: CredentialSchema
      },
      handler: (request, reply) => {
        const {email, password} = request.payload

        return Identity.findOne({ where: { email, provider: 'email' }, include: [ User ] })
          .then(checkIfInstanceExists)
          .then(checkIfEmailIsVerified())
          .then(identity => {
            return server.password.encrypt(password).then(encodedPassword => {
              if (!identity.user.verifyPassword(encodedPassword)) {
                throw Boom.forbidden('Invalid credentials')
              } else {
                return identity.user.issueToken().then(token => {
                  return pick(token.toJSON(), ['userId', 'accessToken'])
                })
              }
            })
          })
          .then(reply)
          .catch(reply)
      }
    }
  })

  server.route({
    method: ['POST'],
    path: `/${User.tableName}/revoke`,
    config: {
      auth: false,
      validate: {
        payload: CredentialSchema
      },
      handler: (request, reply) => {
        const {email} = request.payload

        Identity.findOne({ where: { email } })
          .then(checkIfInstanceExists)
          .then(checkIfEmailIsVerified())
          .tap(identity => {
            identity.emailConfirmationCode = hash(random(8))
          })
          .call('save')
          .tap(identity => {
            server.sendMail('confirm-revoke', {
              subject: 'Confirm revoke',
              email: identity.email,
              data: { identity }
            })
          })
          .then(() => reply().code(204))
          .catch(reply)
      }
    }
  })

  server.route({
    method: ['POST'],
    path: `/${User.tableName}/revoke/{identityId}/{emailConfirmationCode}`,
    config: {
      auth: false,
      validate: {
        params: ActivationSchema
      },
      handler: (request, reply) => {
        Identity.findByPrimary(request.params.identityId, { include: [ User ] })
          .then(checkIfInstanceExists)
          .then(checkIfEmailIsVerified())
          .then(checkConfirmationCode(request.params.emailConfirmationCode))
          .then(issueNewPassword(server))
          .then(() => reply().code(204))
          .catch(reply)
      }
    }
  })
}
