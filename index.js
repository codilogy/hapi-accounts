const Boom = require('boom')
const { resolve } = require('path')

exports.register = (server, options, next) => {
  return server.register([
    require('vision'),
    require('bell'),
    require('hapi-auth-jwt2'),
    {
      register: require('@codilogy/hapi-mailer'),
      options: Object.assign({
        templates: {
          dir: resolve(__dirname, 'templates')
        }
      }, options.mailer)
    },
    {
      register: require('./plugins/password-service'),
      options: {
        pbkdf2Salt: options.pbkdf2Salt
      }
    },
    {
      register: require('./plugins/data-source'),
      options: {
        jwtKey: options.jwtKey
      }
    },
    {
      register: require('./plugins/bell-service'),
      options: options.bell
    }
  ], {
    routes: {
      prefix: options.prefix
    }
  })
  .then(() => {
    const User = server.getModel('user')
    const Identity = server.getModel('identity')
    const validateFunc = (decoded, request, next) => {
      User.findById(decoded.uid, {
        include: [ Identity ]
      }).then(user => {
        if (user) {
          return next(null, user)
        } else {
          return next(Boom.notFound())
        }
      }).catch(next)
    }

    server.auth.strategy('jwt', 'jwt', {
      validateFunc,
      key: options.jwtKey,
      verifyOptions: {
        algorithms: [ 'HS256' ]
      }
    })

    server.auth.default('jwt')

    next()
  })
  .catch(next)
}

exports.register.attributes = {
  pkg: require('./package.json')
}
