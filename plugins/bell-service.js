const path = require('path')
const IS_DEVELOPMENT = process.env.NODE_ENV === 'development'
const buildCredentials = credentials => ({
  provider: String(credentials.provider),
  providerId: String(credentials.profile.id),
  email: credentials.profile.email,
  emailIsVerified: credentials.profile.raw.verified,
  profile: JSON.stringify(credentials.profile),
  credentials: JSON.stringify({
    expiresIn: credentials.expiresIn,
    refreshToken: credentials.refreshToken,
    token: credentials.token,
    query: credentials.query
  })
})

exports.register = (server, options, next) => {
  const providers = Object.keys(options.providers)
  const Identity = server.getModel('identity')
  const User = server.getModel('user')

  /**
   * @private
   * @param identity
   * @returns {*|Promise.<TResult>}
   */
  const createUser = (identity) => {
    return User.create().then(user => {
      return Identity.create(Object.assign(identity, {
        userId: user.id
      })).return(user)
    })
  }

  const findOrCreate = credentials => {
    const identityData = buildCredentials(credentials)

    return Identity.findOne({
      where: {
        provider: identityData.provider,
        email: identityData.email
      },
      include: [ User ]
    }).then(identity => {
      if (!identity) {
        return createUser(identityData)
      } else {
        return identity.user
      }
    }).then(user => {
      return user.issueToken().toJSON()
    })
  }

  const handler = (request, reply) => {
    const {isAuthenticated, credentials} = request.auth

    if (isAuthenticated && credentials) {
      return findOrCreate(credentials).then(token => {
        token.credentials = credentials

        return reply.view('auth', { error: false, debug: IS_DEVELOPMENT, token })
      }).catch(error => {
        return reply.view('auth', { error, debug: IS_DEVELOPMENT })
      })
    } else if (request.auth.error) {
      server.log(request.auth.error)
      return reply.view('auth', {
        error: request.auth.error,
        debug: IS_DEVELOPMENT
      })
    }
  }

  server.views({
    engines: { ejs: require('ejs') },
    relativeTo: path.resolve(__dirname, '../templates')
  })

  /**
   * Catch failing third-party authentications
   * and finalize them without an error for a client.
   */
  server.ext('onPreResponse', (request, reply) => {
    const {response, route} = request
    const [tag, provider] = route.settings.tags || []

    if (response.isBoom && route.method === 'get' && tag === 'auth' && providers.includes(provider)) {
      request.auth.error = response
      request.auth.error.data = request.query

      return handler(request, reply)
    }

    return reply.continue()
  })

  /**
   * Register strategies and routes for each provider
   */
  providers.forEach(provider => {
    server.auth.strategy(provider, 'bell', Object.assign({},
      options.settings,
      options.providers[provider],
      {
        provider
      }
    ))

    server.route({
      method: ['GET', 'POST'],
      path: `/${provider}`,
      config: {
        description: `Authenticate against ${provider} provider`,
        tags: ['auth', provider],
        cache: false,
        auth: {
          mode: 'required',
          strategy: provider
        },
        handler
      }
    })
  })

  next()
}

exports.register.attributes = {
  name: 'accountsBellService'
}
