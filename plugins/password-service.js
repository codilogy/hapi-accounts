const Promise = require('bluebird')
const crypto = require('crypto')

const ITERATIONS = 25000
const KEY_LENGTH = 512
const DIGEST = 'sha512'

class PasswordService {
  constructor (salt) {
    this.salt = salt
  }

  encrypt (password) {
    return new Promise((resolve, reject) => {
      crypto.pbkdf2(password, this.salt, ITERATIONS, KEY_LENGTH, DIGEST, (error, key) => {
        if (error) {
          return reject(error)
        }

        resolve(key.toString('hex'))
      })
    })
  }
}

exports.register = (server, options, next) => {
  server.decorate('server', 'password', new PasswordService(options.pbkdf2Salt))
  next()
}

exports.register.attributes = {
  name: 'accountsPasswordService'
}
