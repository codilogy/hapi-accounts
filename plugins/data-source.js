const path = require('path')

exports.register = (server, options, next) => {
  server.registerModels({
    models: [
      path.resolve(__dirname, '../models/**')
    ]
  }).then(() => {
    server.getModel('token').options.jwtKey = options.jwtKey
  }).then(() => next()).catch(next)
}

exports.register.attributes = {
  name: 'accountsModels'
}
