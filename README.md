# hapi-accounts

[![JavaScript Standard Style](https://img.shields.io/badge/code_style-standard-green.svg)](https://standardjs.com/)
[![Conventional Commits](https://img.shields.io/badge/Conventional%20Commits-1.0.0-green.svg)](https://conventionalcommits.org/)
[![bitHound Overall Score](https://www.bithound.io/bitbucket/codilogy/hapi-accounts/badges/score.svg)](https://www.bithound.io/bitbucket/codilogy/hapi-accounts)
[![Coverage Status](https://coveralls.io/repos/bitbucket/codilogy/hapi-accounts/badge.svg)](https://coveralls.io/bitbucket/codilogy/hapi-accounts)

> Opinionated hapi plugin for injecting accounts management into your API.

## Introduction

You may call this plugin a meta-plugin or component, as it combines several smaller 
and streamline plugins into one bigger functionality. Under the hood we have:

* few Sequelize models with exposed remote methods (routes), 
* JWT token support as the default (and also one and only) authorization method,
* mailer based on Nodemailer with basic templates included (for confirmation mails),
* Bulma support to integrate user's external identities (Facebook, Google+, etc.). 

Not all plugins utilized by accounts component are installed by it. Some of them
have to be installed aside as peer dependencies (check requirements).

## Functionality
 
* Registration with confirmation mail.
* Password revoke.
* Login with other provider (supported by Bulma).
* Accounts linking and unlinking.
* Roles.

## Core concepts

### Definitions

* Account. Combination of User and several Identities, Roles and Tokens it can have.
* User. Unique person which may have several Identities, Roles and Tokens.
* Identity. It is used to authenticate user in system. Identity is related with exactly
one User nad has to be unique for e-mail address and provider name combined. E-mail
address is core value of each Identity.
* Role. Each User may have many Roles, which are used to evaluate an access to
specified resources.
* Token. It belongs to exactly one User, and is used to authorize access to specified
resources. Token is JWT and in its payload it contains User ID.

### Identities are linked by default

When user creates an account for the first time, component creates new user instance
and identity linked with it. At the later time, the same user creates another
account (using for example Facebook). Component has detected that there is already
an identity with the same e-mail address as identity delivered by Facebook provider,
but from different provider.

By default new identity (provided by Facebook), will be attached to the same user
as identity found during registration.

### Identities can be linked manually

In previous situation we have two identities with the same e-mail address. But
in situation when user registered with second identity which has different e-mail,
automatic linking cannot be done. This situation ends up with two different users,
each with one identity.

However there is an option to manually link those identities. In practice this
operation will move identity from one user to another. This operation requires
user to be authenticated to the account which will become its main account.
User must provide token from the identity which has to be moved. An confirmation
mail is send to e-mail address of moved identity. After confirmation identity
is moved to the new user. Second user is not deleted - it is left orphaned.

### E-mail registration requires confirmation

When creating account with e-mail address, this is the only information required.
However, this registration requires confirmation of e-mail address. After successful
confirmation, another e-mail with password is send.

### Passwords are generated automatically

E-mail confirmation and password reset operations end up with an e-mail containing
new password. This enforces strong passwords.

There is also possibility to change password later. This operation requires
positive authorization with token.

## Requirements

- hapi - version 16.x;
- hapi-auth-jwt2 - for access management across API;
- @codilogy/hapi-datasource - provides access to database;

## Usage
 
### Standard account (e-mail)

### Account created by provider (ex. Facebook)

### Joining accounts
 
## License

The MIT License (MIT)

Copyright &copy; 2017 Codilogy, [http://codilogy.com](http://codilogy.com) <info@codilogy.com>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
